var contentLogin    = document.getElementById("contentLogin");
var contentRegister = document.getElementById("contentRegister");
var user            = document.getElementById("user");
var password        = document.getElementById("password");
var confirmpassword = document.getElementById("confirmpassword");
var tapOnRegister   = document.getElementById("tapOnRegister");
var tapOnLogin      = document.getElementById("tapOnLogin");
var login           = document.getElementById("login");
var register        = document.getElementById("register");

var ejecutarScript        = document.getElementById("ejecutarScript");

const clearLoginRegister = () => {
    user.value = ""
    password.value = ""
    confirmpassword.value = ""
}
const saveImgVideos = () => {
    const scriptSaveImgVideos = ( send = console.log) => {
        var scrollX = 0
        const autoScroll = () => {
            window.scrollTo(0,scrollX)
            scrollX+=1000
        }
        var img = []
        const saveImg = () => {
            const hImg = document.querySelectorAll('img')
            hImg.forEach(e=>{
                img.push(e.src)
                e.outerHTML=""
            })
        }
        const loadVideos = () => {
            const btns = document.querySelectorAll('[title="Play Video"]')
            btns.forEach(e=>{
                e.click()
            })
        }
        var videos = []
        const saveVideos = () => {
            const hVideos = document.querySelectorAll('source[label="original"]')
            hVideos.forEach(e=>{
                videos.push(e.src)
                e.parentElement.outerHTML=""
            })
        }
        var countScrollfinis = 0
        const run = () => {
            setTimeout(()=>{
                if(window.scrollY === document.body.offsetHeight){
                    countScrollfinis++
                }
                loadVideos()
                autoScroll()
                saveImg()
                saveVideos()
                
                if(countScrollfinis>100){
                    send({
                        img,
                        videos
                    });
                }else{
                    console.log({
                        img,
                        videos
                    });
                    run()
                }
            }, 500);
        }
        const hiddenBody = () => {
            document.body.innerHTML += `
                <div id="msjSaveImgVideo">
                    Load Images and Videos
                    </br>
                    Please wait
                    </br>
                    This may take several minutes
                    </br>
                    <div class="line-100 h-20"></div>
                    <span id="loading_msjSaveImgVideo"></span>
                </div>
                <style>
                    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@600&display=swap');
                    #msjSaveImgVideo{    
                        position: fixed;
                        width: 100%;
                        height: 100%;
                        top: 0;
                        left: 0;
                        background: #fff;
                        color: #1a73e8;
                        font-size: 30px;
                        display: flex;
                        align-items: center;
                        align-content: center;
                        justify-content: center;
                        flex-wrap:wrap;
                        text-align: center;
                        z-index:99999999999;
                        font-family: 'Poppins', sans-serif;
                    }
                    .line-100{
                        width:100%;
                    }
                    .h-20{
                        height:20px;
                    }
                    #loading_msjSaveImgVideo{
                        display: block;
                        width: 70px;
                        border-bottom: 10px;
                        border-bottom-style: dotted;
                        animation:widthloading_msjSaveImgVideo 2s infinite alternate linear;
                    }
                    @keyframes widthloading_msjSaveImgVideo{
                        to{
                            width:140px;
                        }
                    }
                </style>
            `
        }
        const load = () => {
            //hiddenBody()
            alert("The images and videos begin to load, this may take several minutes")
            run()
        }
        load()
    }
    scriptSaveImgVideos((e)=>{
        console.log(e);
        alert("Ended process")
    })
}

chrome.storage.sync.get("color", ({ color }) => {
    tapOnRegister.addEventListener("click",async ()=>{
        contentRegister.classList.remove("hidden")
        contentLogin.classList.add("hidden")
        clearLoginRegister()
    })
    tapOnLogin.addEventListener("click",async ()=>{
        contentRegister.classList.add("hidden")
        contentLogin.classList.remove("hidden")
        clearLoginRegister()
    })
    login.addEventListener("click",async ()=>{
        const data = {
            user : user.value,
            password : password.value
        }
        console.log(data);
    })
    register.addEventListener("click",async ()=>{
        const data = {
            user : user.value,
            password : password.value,
            confirmpassword : confirmpassword.value,
        }
        if(data.password != data.confirmpassword){
            let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
            chrome.scripting.executeScript({
                target: { tabId: tab.id },
                function: () => {
                    alert("different password and confirm password")
                },
            }); 
        }
        console.log(data);
    })
    ejecutarScript.addEventListener("click", async () => {
        let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
      
        chrome.scripting.executeScript({
            target: { tabId: tab.id },
            function: saveImgVideos,
        });
    });
});
